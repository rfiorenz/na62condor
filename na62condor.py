#!/usr/bin/python3

import os
import re
import os.path
import sys
import htcondor
import bisect
import glob
import shutil
from configparser import ConfigParser, NoOptionError
import itertools

from helper import check_required_options, check_files_exist, official_list_path, period_runs

def get_range(str_):
    '''Return the str_ without the range, and the range.
    If no range found, the range is None.
    Otherwise, the range is a 2-tuple containing the two limits.
    If single number found, the range is (N, N+1).
    If the lower limit is not found, it is assumed to be 0.
    If the upper limit is not found, it is assumed to be inf.'''
    if ':' not in str_:
        return str_, None
    
    split_str = str_.rsplit(':', 1)
    range_str = split_str[-1]
    
    if '-' not in range_str:
        return split_str[0], (int(range_str), int(range_str) + 1)
    
    part = range_str.partition('-')
    return split_str[0], (int(part[0]) if part[0] else 1, int(part[2]) if part[2] else float('inf'))

def get_ranges(list_file, range_, files_per_job):
    # there are surely smarter ways to do this, but anyway I/O is minimized here
    
    def chunks(lst, n):
        """Yield successive n-sized chunks from lst."""
        for i in range(0, len(lst), n):
            yield lst[i:i + n]
        
    with open(list_file) as f:
        f.seek(0)
        lines_to_use = [i_line + 1 for i_line, _ in enumerate(f) if range_[0] <= i_line + 1 <= range_[1]]
    
    return [(c[0], c[-1]) for c in chunks(lines_to_use, files_per_job)]
        


def find_run_list(run, filter, version):
    period = list(period_runs.values())[
        bisect.bisect_right(list(period_runs.keys()), run) - 1]
    if period is None:
        return None
    lists = glob.glob(os.path.join(official_list_path, period.rstrip('.'.join([chr(x) for x in range(ord('A'), ord('Z'))])), period) + f"-{version}*/*{str(run).zfill(5)}*.{filter.upper()}*")
    return max(lists) if lists else None # return latest processed

def period_to_run_range(min_period, max_period):
    min_index, max_index = [list(period_runs.values()).index(period) for period in (min_period, max_period)]
    runs = list(period_runs.keys())
    min_run = runs[min_index]
    max_run = runs[max_index + 1] - 1
    return min_run, max_run

def find_run_lists(min_run, max_run, filter, version):
    min_period_id, max_period_id = (bisect.bisect_right(
        list(period_runs.keys()), run) - 1 for run in (min_run, max_run))
    min_period_id = max(1, min_period_id)
    max_period_id = min(len(period_runs) - 2, max_period_id)

    def get_run_id(x):
        try:
            return int(os.path.basename(x)[3:9])
        except ValueError:
            return -1
    return [
        max(v) for _, v in itertools.groupby(
            sorted(
                [run
                    for period in tuple(period_runs.values())[min_period_id:max_period_id + 1]
                 for run in glob.glob(os.path.join(official_list_path, period[:4], period) + f"-{version}*/*.{filter.upper()}*")
                 if min_run <= get_run_id(run) <= max_run],
                key=get_run_id),
            key=get_run_id
        )
    ]

config = ConfigParser()
config.read(sys.argv[1:])

for batch_name in config.sections():
    batch = config[batch_name]

    # remove empty options
    for option in batch:
        if not batch[option]:
            config.remove_option(batch_name, option)

    print(f"\n##### PROCESSING BATCH: {batch_name}")

    if not check_required_options(batch, ('outputLogs', 'outputPath', 'envFile', 'executable')):
        continue

    # parse options that are common for all runs
    input_path = os.path.abspath(batch.get('inputPath', os.getcwd()))
    input_lists = [os.path.abspath(list_) for glob_ in batch.get('inputLists', '').split()
                                        for list_ in glob.glob(os.path.join(input_path, glob_))]
    outlogs_path = os.path.abspath(batch['outputLogs'])
    outfiles_path = os.path.abspath(batch['outputPath'])
    env_file = os.path.abspath(batch['envFile'])
    executable = os.path.abspath(batch['executable'])
    arguments = batch.get('arguments', '').replace(
        '"', '""').replace("'", "''")
    script_file = os.path.abspath(
        batch.get('script', './condor_executable.sh'))

    redirect_stderr = batch.getboolean('redirectStdErr', True)
    check_integrity = batch.get('checkIntegrity', '').replace(
        '"', '""').replace("'", "''")
    max_retries = batch.getint('maxRetries', 3 if check_integrity else 0)
    
    transfer_input_files = [os.path.abspath(f)
                            for glob_ in batch.get('transferInputFiles', '').split() for f in glob.glob(glob_)
                            if os.path.isfile(os.path.abspath(f)) or print(f"WARNING: {f} is not an existing file")]

    # find lists for runs specified in the runs option
    for run_filter in batch.get('runs', '').split():
        # remove the range, convert the run to a list file, then readd the range at the end
        run_filter, delimiter, range_str = run_filter.partition(':')
        range_str = delimiter + range_str

        # what filter?
        if '.' in run_filter: # specified in the runs option: 9001.PNN
            run, filter_ = run_filter.split('.', 1)
        else: # not found in the runs option, search for it in the filter option
            run = run_filter
            try:
                filter_ = batch['filter']
            except (NoOptionError, KeyError):
                print(
                    f"No filter specified for run(s) {run}. Please use the format 9001.PNN, or specify a filter with the filter option.")
                continue
        version = batch.get('version', 'v')
        if run.isdigit(): # single run
            l = find_run_list(int(run), filter_, version)
            if l is None:
                print(f"Could not find list for run {run}, filter {filter_}{f', version {version}' if batch.get('version', '') else ''}. Skipping...")
                continue
            input_lists.append(find_run_list(int(run), filter_, version) + range_str)
        elif run in period_runs.values(): # single period
            input_lists += [l + range_str for l in find_run_lists(*period_to_run_range(run, run), filter_, version)]
        elif run.count('-') == 1: # range of runs or periods
            min_, max_ = run.split('-', 1)
            if min_.isdigit() and max_.isdigit(): # range of runs
                input_lists += [l + range_str for l in find_run_lists(*[int(x)
                                            for x in run.split('-', 1)], filter_, version)]
            elif min_ in period_runs.values() and max_ in period_runs.values():
                input_lists += [l + range_str for l in find_run_lists(*period_to_run_range(min_, max_), filter_, version)]
            else:
                print(f"{run} is not a valid range of runs (e.g. 9001-9010) or periods (e.g. 2021E-2021A). Skipping...")
                continue
        else:
            print(f"{run} is not a valid run (e.g. 9001), period (e.g. 2018E), or range of runs (e.g. 9001-9010) or periods (e.g. 2021E-2021A). Skipping...")
            continue
            

    if not check_files_exist(script_file, env_file, executable):
        continue

    itemdata = []
    runs = []
    overwrite_all = False

    separate_batches = batch.getboolean(
        'separateBatches', len(input_lists) > 1)
    
    for list_file in input_lists:
        # build a short name for the list, append it to the runs list
        run_base = os.path.basename(os.path.splitext(list_file.split(':')[0])[0]) # remove the range to calculate the basename
        match = re.search('Run(\d+)', run_base, re.IGNORECASE)
        if match and 'mc' not in run_base:  # get only run number, but not if mc
            run_base = match.group(1).lstrip('0')
        i_try = 1
        run = run_base
        while run in runs:
            i_try += 1
            run = f"{run_base}_{i_try}"
        runs.append(run)

    '''TODO:
    at this point, check if one job per list (currently, filesPerJob <= 0).
    if so, make the jobs in an easier way... (and remove useless folder per single job)
    ideally, add a different option listsPerJob
    '''

    assert(len(input_lists) == len(runs))
    for list_file, run in zip(input_lists, runs):
        run_batch_name = f"{batch_name}_{run}" if separate_batches else batch_name
        outfiles_dir = os.path.join(outfiles_path, run)
        outlogs_dir = os.path.join(outlogs_path, run)

        list_file, range_ = get_range(list_file)
        if range_ is None:
            _, range_ = get_range(':' + batch.get('range', '-'))

        print(f"\nList file: {list_file}\nShort name: {run}")
        if not os.path.exists(list_file):
            print(f"List file not found, skipping...")
            continue

        print(f"Logs dir: {outlogs_dir}\nOutput dir: {outfiles_dir}")
        if os.path.isdir(outlogs_dir) or os.path.isdir(outfiles_dir):
            answer = ""
            while not overwrite_all:
                answer = input(
                    "The above paths already exist. Are you sure to overwrite them? (y/n/a) ").lower()
                if answer not in ('y', 'n', 'a'):
                    print(
                        "Please answer y for yes, n for no, a for yes to all in this batch.")
                else:
                    break
            if answer == 'n':
                print(f"Ok, skipping list {run}...")
                continue
            elif answer == 'a':
                overwrite_all = True
                print("Ok, overwriting all directories...")
            for this_dir in (outlogs_dir, outfiles_dir):
                if os.path.isdir(this_dir):
                    shutil.rmtree(this_dir)

        try:
            os.makedirs(outlogs_dir)
            os.makedirs(outfiles_dir)
        except FileExistsError:
            print("Cannot remove existing directories (this may happen when AFS is slow). Please retry.")
            sys.exit(1)

        if range_[0] >= range_[1]:
            print(f"Invalid range, skipping...")
            continue
        elif range_ != (1, float('inf')):
            print(f"Using files from {range_[0]} {('to ' + str(range_[1])) if range_[1] != float('inf') else 'onwards'}")

        files_per_job = batch.getint('filesPerJob', 1)
        if files_per_job <= 0:
            files_per_job = sys.maxsize
        
        list_file = shutil.copy2(list_file, outlogs_dir)
        ranges = get_ranges(list_file, range_, files_per_job)
        if len(ranges) == 0:
            print(f"No files found for this list, skipping...")
            continue

        print(
            f"There will be {len(ranges)} jobs for this list, with batchname {run_batch_name}")

        output_base = os.path.join(outlogs_dir, run)
        n_digits = len(str(len(ranges)))
        def single_file_itemdata(i_job, line_min, line_max):
            filename = output_base + '_' + str(i_job).zfill(n_digits)
            filename_base = os.path.basename(filename)
            command = f"./{os.path.basename(executable)} -l {os.path.basename(list_file)} -o {filename_base}.root {arguments}"
            files_to_transfer = f'{filename_base}.root {batch.get("outputFiles", "")}'
            check_integrity_command = check_integrity.replace(
                r'$file', f'{os.path.join(outfiles_dir, filename_base)}.root')

            return {
                'output': filename + '.out',
                'error': filename + ('.out' if redirect_stderr else '.err'),
                'log': (filename if batch.get('separateLogs', False) else output_base) + '.log',
                'JobBatchName': run_batch_name,
                'arguments': f'"\'{env_file}\' {line_min} {line_max} \'{command}\' \'{check_integrity_command}\' {max_retries} \'{outfiles_dir}\' \'{files_to_transfer}\'"',
                'transfer_input_files': ', '.join([executable, list_file] + transfer_input_files),
            }

        itemdata += [single_file_itemdata(i_job, line_min, line_max) for i_job, (line_min, line_max) in enumerate(ranges)]

    if len(itemdata) == 0:
        print("No jobs to submit, skipping this batch...")
        continue

    try:
        print("\n----- End list processing, start talking with HTCondor")
        print("\nAdding credentials...")
        col = htcondor.Collector()
        credd = htcondor.Credd()
        credd.add_user_cred(htcondor.CredTypes.Kerberos, None)

        schedd = htcondor.Schedd()
        print(
            f"Using schedd host: {re.search('alias=([^&]*)', schedd.location.address).group(1)}")

        sub = htcondor.Submit({
            'priority' : batch.get('priority', 0),
            'executable': script_file,
            'MY.SendCredential': True,
            'RequestCpus': batch.get('numberOfCPUs', 1),
            'MY.WantOS': '"' + batch.get('operatingSystem', 'el9') + '"',
            '+JobFlavour': '"' + batch.get('jobFlavour', 'espresso') + '"',
            'transfer_output_files': '""'  # we're already doing this manually...
        })

        print(f"\nSubmitting {len(itemdata)} jobs...")
        count_jobs = 0
        
        for run in runs:
            run_batch_name = f"{batch_name}_{run}" if separate_batches else batch_name
            if run:
                run = '_' + run
            itemdata_to_submit = [x for x in itemdata if x['JobBatchName'] == run_batch_name]
            if not itemdata_to_submit:
                continue
            print(f"Submitting jobs with batchname {run_batch_name}...")
            for attempt in range(5):
                try:
                    result = schedd.submit(sub, itemdata=(x for x in itemdata_to_submit))
                    n_jobs = result.num_procs()
                except htcondor.HTCondorIOError as e:
                    if attempt < 4:
                        print(f"Failed with HTCondorIOError: {e}\nRetrying... ({attempt+2} / 5)")
                    else:
                        raise e
                else:
                    break
            print(
                f"Submitted {n_jobs} jobs with clusterID {result.cluster()}.")
            count_jobs += n_jobs
            if not separate_batches:
                break

        print(f"\n##### {count_jobs} jobs submitted in total for batch {batch_name}")
    except htcondor.HTCondorException as e:
        print(f"\n\nError! Stopping...\n{type(e).__name__}: {e}\nMaybe use the command \"myschedd bump\" and retry, and/or retry in a few minutes?")

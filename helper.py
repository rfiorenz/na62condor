import os.path
import htcondor
import subprocess

official_list_path = "/eos/experiment/na62/data/offline/lists/Data/"

period_runs = {
    0: None,
    6278: "2016A",
    7615: "2017D",
    7747: "2017C",
    7876: "2017B",
    8134: "2017A",
    8518: "2018A",
    8548: "2018B",
    8777: "2018C",
    8805: "2018D",
    8968: "2018E",
    9047: "2018F",
    9147: "2018G",
    9306: "2018H",
    10787: "2021E",
    10885: "2021D",
    10915: "2021C",
    11000: "2021B",
    11061: "BeamDump2021",
    11096: "2021A",
    11741: "2022A",
    12013: "2022B",
    12165: "2022C",
    12340: "2022D",
    12464: "2022E",
    12994: "2023A",
    13206: "2023B",
    13284: "2023C",
    13416: "BeamDump2023",
    13472: "2023D", # run 13438 is also part of 2023D, but that can't work here...
    13523: "2023E",
    13622: "2023F",
    13800: "2024",
    15000: None,
}


def check_required_options(batch, options):
    for option in options:
        if option not in batch:
            print(
                f"Required option {option} was not supplied. Skipping this batch...")
            return False
    return True


def check_files_exist(*args):
    for f in args:
        if not os.path.isfile(f):
            print(f"File {f} not found! Skipping this batch...")
            return False
    return True

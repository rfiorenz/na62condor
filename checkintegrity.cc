#include <TDirectory.h>
#include <TEnv.h>
#include <TError.h>
#include <TFile.h>
#include <TString.h>
#include <TTree.h>
#include <dirent.h>
#include <functional>
#include <iostream>
#include <string>

// executes cb over all files in path, recursively (unused)
void listFiles(const std::string& path, std::function<void(const std::string&)> cb) {
  if (auto dir = opendir(path.c_str())) {
    while (auto f = readdir(dir)) {
      if (f->d_name[0] == '.')  // should check for f->d_name != NULL before, but ROOT complains...
        continue;
      if (f->d_type == DT_DIR) {
        std::string newpath = path + "/" + f->d_name + "/";
        std::cout << "Entering " << newpath << std::endl;
        listFiles(newpath, cb);
      }

      if (f->d_type == DT_REG)
        cb(path + f->d_name);
    }
    closedir(dir);
  }
}

int checkintegrity(const std::string& path, TString objectToValidate, bool checkTree = false) {
  gErrorIgnoreLevel = kError;
  gEnv->SetValue("TFile.Recover", 0);

  TString filepath = path;
  if (filepath.BeginsWith("/eos/user") || filepath.BeginsWith("/eos/home"))
    filepath = "root://eosuser.cern.ch/" + filepath;
  else if (filepath.BeginsWith("/eos/experiment"))
    filepath = "root://eospublic.cern.ch/" + filepath;
  std::cout << "Validating file " << filepath << " by checking " << objectToValidate << " is a non-empty " << (checkTree ? "TTree." : "TDirectory.")
            << std::endl;

  if (!filepath.EndsWith(".root")) {
    std::cout << "Doesn't look like a root file. Ok, I guess" << std::endl;
    return 0;
  }

  std::cout << "Opening file..." << std::endl;

  auto file = new TFile(filepath);
  if (!file || file->IsZombie()) {
    std::cout << "ERROR: File is unreadable" << std::endl;
    file->Close();
    return 1;
  }

  bool valid = false;
  if (checkTree) {
    std::cout << "Checking tree " << objectToValidate << std::endl;
    auto tree = file->Get<TTree>(objectToValidate);
    valid = tree && tree->GetEntries();
  } else {
    std::cout << "Checking directory " << objectToValidate << std::endl;
    auto dir = file->Get<TDirectory>(objectToValidate);
    valid = dir && dir->GetListOfKeys()->GetEntries();
  }

  if (!valid) {
    std::cout << "ERROR: " << objectToValidate << " not found or empty" << std::endl;
    file->Close();
    return 2;
  }

  std::cout << "File is good." << std::endl;
  file->Close();
  return 0;
}

int main(int argc, char* argv[]) {
  if (argc != 3 && argc != 4) {
    std::cout << "Bad number of arguments" << std::endl;
    return 3;
  }

  return checkintegrity(std::string(argv[1]), std::string(argv[2]), argc == 4);

  // THIS CODE SEARCHES DIRECTORY argv[1] RECURSIVELY
  // char *dirToSearch = argv[1];
  // char *dirToValidate = argv[2];

  // listFiles(dirToSearch, [&](const std::string &path)
  //           { checkintegrity(path, dirToValidate); });
}
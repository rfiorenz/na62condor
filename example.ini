###### Format of this file:
# see https://docs.python.org/3/library/configparser.html#supported-ini-file-structure
###### Required:
# outputLogs: path where to put log files (will be created if not exists). Should be on AFS
# outputPath: path where to put output files (will be created if not exists). Should be on EOS
# envFile: path to scripts/env.sh
# executable: path to your executable file (can be an analysis, or NA62Reco, or NA62MC)
###### Optional:
# runs: list of space separated runs and filters to process. Format: 9001.PNN, or 10775-10858.PNN to select a range of runs, or 2018D.PNN to select a whole sample, or 2021E-2021C.PNN to select a range of samples. The latest processed list will be used for each run
# filter: default filter to use for the runs option, when not specified. see example below
# version: version of the framework used to process the data specified in the runs option (defaults to latest one)
# inputLists: list of space separated paths of custom lists to process. Paths can be relative to inputPath, or absolute. Supports globs (ie wildcards *, ? etc)
# inputPath: path where to search for lists (defaults to current working directory)
# range: range of files to process from each list. Can be either a single number (to process a single file from the list), or a range in the format N-M. Leave out N to set it to 1, leave out M to set it to the end of the list. Appending the range prefixed with ':' to a list or run will override the range for that list or run, see example below
# arguments: extra arguments to pass to the executable (example: -e 1 -p analyzer1:parameter1=0;parameter2=1&analyzer2:param=0)
# outputFiles: extra files, other than the output ROOT file, that are produced by your executable and that you want to get (one per job). Supports globs (ie wildcards *, ? etc). Be careful: you don't want to copy a lot of junk to your outputPath! Specify the filename as closely as possible
# filesPerJob: number of files per job. Set to any nonpositive integer to have only one job per list. Defaults to 1
# separateBatches: whether to have different batch names for each list analyzed (defaults to yes)
# separateLogs: whether to have different HTCondor log files for each job (defaults to no)
# script: path to condor_executable.sh, defaults to ./condor_executable.sh
# checkIntegrity: command to execute after the job ($file will be substituted with the output ROOT file absolute path). if this does not have 0 exit code, then the job will be executed again
# maxRetries: max retries for checkIntegrity, defaults to 3
# jobFlavour: see HTCondor docs.
#       Available job flavours:
#       espresso     = 20 minutes (default)
#       microcentury = 1 hour
#       longlunch    = 2 hours
#       workday      = 8 hours
#       tomorrow     = 1 day
#       testmatch    = 3 days
#       nextweek     = 1 week
# numberOfCPUs: number of CPUs to request, defaults to 1. Every CPU you request corresponds to 2GB of RAM and 20GB of disk space, see batchdocs.web.cern.ch
# priority: job priority (see HTCondor docs), defaults to 0
# operatingSystem: el7, el8 or el9 (default)
# redirectStdErr: redirect stderr to stdout, producing one single file instead of splitting (defaults to yes)
# transferInputFiles: any additional file that will be transferred to the condor node. Supports globs (ie wildcards *, ? etc). Be careful: you don't want to copy a lot of junk to the condor node! Specify the filename as closely as possible

# ↓ [this] gives the batchname
[myAnalysis]
inputPath= /eos/home-u/username/mylists/
inputLists= private1.list private2.list:7
runs= 10775.PNN 10769 10952-10957.RES3TV:50-
filter= MUONNU
# Lists that will be processed:
# /eos/home-u/username/mylists/private1.list
# /eos/home-u/username/mylists/private2.list (only the 7th file will be processed)
# Latest processed list of run 10775, PNN-filtered
# Latest processed list of run 10769, MUONNU-filtered
# Latest processed lists of all runs between 10952 and 10957, RES3TV-filtered (only from the 50-th file onwards for each run)

outputLogs= /afs/cern.ch/user/u/username/logs
outputPath= /eos/home-u/username/output/
envFile= /afs/cern.ch/work/u/username/na62fw/NA62Analysis/scripts/env.sh
executable= /afs/cern.ch/work/u/username/userdir/bin-cc7/myAnalysis

# will get all .dat files and all files starting with "LKr" into the output path. (don't do this! Rather, specify exact filenames of what you need)
outputFiles=*.dat LKr*

# this will check that output files are readable and the top-level TDirectory called MyAnalyzer is existing and not empty
checkIntegrity= root -q -l '/INSERT/PATH/TO/na62condor/checkintegrity.cc("$file", "MyAnalyzer")'

arguments= -e 1 -C /afs/cern.ch/work/n/na62prod/public/CDB/2021A-v3.0/
jobFlavour = longlunch


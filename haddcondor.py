#!/usr/bin/python3

import os
import os.path
import sys
import htcondor
import glob
import shutil
from configparser import ConfigParser
import math
import re

from helper import check_required_options, check_files_exist

config = ConfigParser()
config.read([x for x in sys.argv[1:]])

for batch_name in config.sections():
    batch = config[batch_name]

    # remove empty options
    for option in batch:
        if not batch[option]:
            config.remove_option(batch_name, option)

    print(f"\nProcessing batch: {batch_name}")

    if not check_required_options(batch, ('inputPath', 'outputLogs')):
        continue

    # parse options that are common for all runs
    input_path = os.path.abspath(batch['inputPath'])
    dirs_to_add = [os.path.relpath(dir_, input_path) for glob_ in batch.get('dirsToAdd', '*').split() for dir_ in glob.glob(os.path.join(input_path, glob_, ""))]
    outlogs_path = os.path.abspath(batch['outputLogs'])
    outfiles_path = os.path.abspath(
        batch.get('outputPath', os.path.join(input_path, 'hadd')))
    root_dir = os.path.abspath(batch['rootdir']) if 'rootdir' in batch else max(glob.glob(
        '/cvmfs/sft.cern.ch/lcg/app/releases/ROOT/[0-9].[0-9][0-9].[0-9][0-9]/x86_64-almalinux9*-gcc114-opt/'))
    env_file = os.path.join(root_dir, 'bin/thisroot.sh')
    executable = os.path.join(root_dir, 'bin/hadd')
    arguments = batch.get('arguments', '-k -f')
    if arguments == '--':
        arguments = ''
    script_file = os.path.abspath(
        batch.get('script', './condor_executable.sh'))
    redirect_stderr = batch.getboolean('redirectStdErr', True)
    check_integrity = batch.get('checkIntegrity', '').replace(
        '"', '""').replace("'", "''")
    max_retries = batch.getint('maxRetries', 3 if check_integrity else 0)

    if not check_files_exist(script_file, env_file, executable):
        continue

    print(f"Using root installation at {root_dir}")

    print(f"Logs dir: {outlogs_path}\nOutput dir: {outfiles_path}")
    if os.path.isdir(outlogs_path) or os.path.isdir(outfiles_path):
        answer = ""
        while True:
            answer = input(
                "The above directories already exist. Remove the directories, Overwrite existing files, or Skip this batch? (r/o/s) ").lower()
            if answer not in ('r', 'o', 's'):
                print(
                    "Please answer r to remove the directories, o to overwrite existing files, s to skip the batch.")
            else:
                break
        if answer == 's':
            print(f"Ok, skipping batch {batch_name}...")
            continue
        elif answer == 'r':
            for this_dir in (outlogs_path, outfiles_path):
                if os.path.isdir(this_dir):
                    shutil.rmtree(this_dir)

    for this_dir in (outlogs_path, outfiles_path):
        if not os.path.isdir(this_dir):
            os.makedirs(this_dir)

    if 'nJobs' in batch:
        print(f"\nUsing hadd parallelization in {batch['nJobs']} processes")
    else:
        print("\nUsing dynamic hadd parallelization")

    itemdata = []
    print("Directories that will be added:")
    for run in dirs_to_add:
        dir_to_add = os.path.join(input_path, run)
        if dir_to_add == outfiles_path:
            continue

        print(dir_to_add, end='')
        if not os.path.exists(dir_to_add):
            print(f" -> ERROR: Not found, skipped")
            continue

        root_files_glob = f"{dir_to_add}/*.root"
        if not any(glob.iglob(root_files_glob)):
            print(" -> ERROR: No root files found, skipped")
            continue

        log_file_basename = os.path.join(outlogs_path, run)
        n_jobs = batch.getint('nJobs', int(
            math.sqrt(len(glob.glob(root_files_glob)))))
        output_filename = run + ".root"
        command = f'{executable} {arguments}{f" -j {n_jobs}" if n_jobs > 1 else ""} {output_filename} {root_files_glob}'
        check_integrity_command = check_integrity.replace(
            r'$file', os.path.join(outfiles_path, run + ".root"))

        itemdata.append({
            'output': log_file_basename + '.out',
            'error': log_file_basename + ('.out' if redirect_stderr else '.err'),
            'log': log_file_basename + '.log',
            'arguments': f'"{env_file} 0 0 \'{command}\' \'{check_integrity_command}\' {max_retries} \'{outfiles_path}\' \'{output_filename}\'"'
        })
        print()

    if len(itemdata) == 0:
        print("No jobs to submit, skipping this batch...")
        continue

    try:
        print("\nAdding credentials...")
        col = htcondor.Collector()
        credd = htcondor.Credd()
        credd.add_user_cred(htcondor.CredTypes.Kerberos, None)

        schedd = htcondor.Schedd()
        print(
            f"Using schedd host: {re.search('alias=([^&]*)', schedd.location.address).group(1)}")

        sub = htcondor.Submit({
            'priority' : batch.get('priority', 0),
            'executable': script_file,
            'MY.SendCredential': True,
            'RequestCpus': batch.get('numberOfCPUs', 1),
            'MY.WantOS': '"' + batch.get('operatingSystem', 'el9') + '"',
            '+JobFlavour': '"' + batch.get('jobFlavour', 'espresso') + '"',
            'JobBatchName': batch_name,
            'transfer_output_files': '""',  # we're already doing this manually...
        })

        print(f"Submitting {len(itemdata)} jobs...")
        for attempt in range(5):
            try:
                with schedd.transaction() as txn:
                    result = sub.queue_with_itemdata(txn, itemdata=iter(itemdata))
            except htcondor.HTCondorIOError as e:
                print(f"Failed with HTCondorIOError: {e}\nRetrying... ({attempt+1} / 5)")
            else:
                break
        
        print(f"Submitted {result.num_procs()} jobs to cluster {result.cluster()}.")

    except htcondor.HTCondorException as e:
        print(f"{type(e).__name__}: {e}")

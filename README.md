## Installation

First, try to run
    
    $ ./na62condor.py

or

    $ python3 na62condor.py

If you get no error, then you don't need any installation (this should be the case on a "clean" shell where no env files have been sourced). Otherwise, you can use a python virtual environment, as described in the following section.

### Using a python virtual environment

Create a python virtual environment in any path you like (here it will be `myvenv`):

    $ python3 -m venv myvenv
	$ source myvenv/bin/activate

Install `htcondor` in your virtual environment:

	(myvenv) $ pip install htcondor

You can now use `na62condor.py` and `haddcondor.py` as described in the Usage section.

When you're done using your virtual environment, you can deactivate it with

    (myvenv) $ deactivate

## Updating

A simple `git pull` will do the trick.

Please update often - I am actively maintaining this software, fixing bugs and adding features whenever I need it.

If you encounter any problem please open an issue, or a MR. (Or feel free to just contact me privately.)

## Usage

### For a custom executable
Create your `.ini` file (you can find an example with documentation in `example.ini`), then execute

    $ python3 na62condor.py path/to/your/file.ini

### Jobs to `hadd` multiple directories

Create your `.ini` file (you can find an example with documentation in `example_hadd.ini`), then execute

    $ python3 haddcondor.py path/to/your/file.ini

### Multiple batches

Both scripts support multiple batches: you can have multiple `[sections]` inside your `.ini` file, and/or call them with multiple `.ini` files, such as

    $ python3 na62condor.py file1.ini file2.ini

Every `[section]` in every file will be processed separately as a batch to submit.

You can use the special `[DEFAULT]` section to set default values for the variables in all other sections, and you are free to define any variable you like for use in substitutions. See https://docs.python.org/3/library/configparser.html#supported-ini-file-structure for more details.

#!/bin/bash

nExpectedArgs=8

if [ $# != $nExpectedArgs ]; then
    echo "Expected $nExpectedArgs arguments, got $#. Exiting..."
    exit 1
fi

envFile=$1
lineMin=$2
lineMax=$3
commandToExecute=$4
checkIntegrity=$5
maxRetries=$6
outFilesDir=$7
filesToTransfer=$8

function executeAndTransfer() {
    echo "[$(date)] Starting execution."
    $commandToExecute
    exitCode=$?
    echo ""
    echo "[$(date)] Command finished with exit code $exitCode"

    echo "[$(date)] ls -lhA:"
    ls -lhA

    # now we have to find and transfer output files.

    # first retrieve some basename ID of the file
    fileBasename="${listFile%.*}"

    # actual file transfer
    for fileToTransfer in $filesToTransfer; do
        if [[ -e "$fileToTransfer" ]]; then
            # make sure it contains the basename, if available
            if [[ ! -z "${fileBasename}" && ! "$fileToTransfer" == *"${fileBasename}"* ]]; then
                newFilename="${fileToTransfer%.*}_${fileBasename}.${fileToTransfer##*.}"
                mv "${fileToTransfer}" "${newFilename}"
                fileToTransfer="${newFilename}"
            fi
            # add protocol
            if [[ "$outFilesDir" == "/eos/home"* ]]; then  # /eos/home- paths don't exist in root://eosuser.cern.ch/
                outFilesDir="${outFilesDir/\/eos\/home-/\/eos\/user\/}"
            fi
            if [[ "$outFilesDir" == "/eos/user"* ]]; then
                outFilesDir="root://eosuser.cern.ch/$outFilesDir"
            elif [[ "$outFilesDir" == "/eos/experiment"* ]]; then
                outFilesDir="root://eospublic.cern.ch/$outFilesDir"
            fi
            echo "[$(date)] Transferring $fileToTransfer to $outFilesDir"
            xrdcp --nopbar --force --verbose "$fileToTransfer" "$outFilesDir"
        else
            echo "[$(date)] WARNING: No $fileToTransfer found!"
        fi
    done
}

echo "[$(date)] Job started."
echo "[$(date)] Env file: $envFile"
source $envFile

# first find the list
shopt -s nullglob  # so that next line returns nothing if no list files are there
listFiles=( *.list )
if [ ${#listFiles[@]} -gt 1 ]; then
    echo "[$(date)] WARNING: Multiple .list files found! Using ${listFiles[0]}"
fi
listFile=${listFiles[0]}

# use only the specified range from the list file
if [[ ! -z $listFile ]]; then
    echo "[$(date)] List file: $listFile; I will use the line range: ${lineMin}-${lineMax}"
    # TODO: sanity check of lineMin and lineMax
    sed -i "${lineMin}"',$!d;'"${lineMax}q" $listFile
    echo "[$(date)] First and last lines used:"
    (head -1; tail -1) < $listFile
fi

echo "[$(date)] The command I will execute is: "
echo "$commandToExecute"

if [ $maxRetries -gt 0 ]; then
    echo "[$(date)] I will also check for file integrity, for $maxRetries times max, with the command:"
    echo $checkIntegrity
fi

executeAndTransfer

# check integrity.
if [ $maxRetries -gt 0 ]; then
    retries=0
    while :; do
        echo ""
        echo "[$(date)] Checking integrity with command:"
        echo $checkIntegrity

        if eval $checkIntegrity; then
            echo ""
            echo "[$(date)] Integrity test passed! Exiting."
            exit 0
        fi

        echo ""
        (( retries++ ))
        if [ $retries -le $maxRetries ]; then
            echo "[$(date)] Integrity test failed, retrying... ($retries/$maxRetries)"
            executeAndTransfer
        else
            echo "[$(date)] Integrity test failed, stopping at $maxRetries retries."
            exit 2
        fi
    done
fi